# MR widget with malformed reports

- WHEN there is no malformed report artifact for both default branch and merge request branch
    - Run pipeline for default branch without setting and CI variable
    - Create a merge request for a branch containing random changes in the README file.
    - Verify that the MR security report widget shows an added vulnerability**(Which is correct)**.

- WHEN the default branch pipeline has malformed artifact
    - Run pipeline with `USE_MALFORMED_REPORT` CI variable set as `true` for the default branch to produce malformed report artifact.
    - Create a merge request for a branch containing random changes in the README file.
    - Verify that the MR security report widget does not show any added/fixed vulnerabilities**(Which is wrong)**.

- If the merge request pipeline has malformed artifact
    - Run pipeline without setting the `USE_MALFORMED_REPORT` CI variable for the default branch.
    - Create a merge request for a branch containing random changes in the README file. Make sure the latest commit message on that branch contains `USE_MALFORMED_REPORT` text.
    - Verify that the MR security report widget does not show any added/fixed vulnerabilities**(Which is wrong)**.
